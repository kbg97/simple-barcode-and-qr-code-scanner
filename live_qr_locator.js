const codeReader = new ZXing.BrowserQRCodeReader();

$(".controls").on("click", "button.start-qr", function(e) {
    e.preventDefault();
    $("div#interactive").html('<video id="qr-video-preview"></video>');
    $("div#interactive").addClass('full-screen');
    codeReader.decodeFromVideoDevice(null, 'qr-video-preview', (result, err) => {
        if (result) {
            $("#qr-result").val(result.text);
            codeReader.stopStreams();
            setTimeout(() => {
                $("div#interactive").html('');
                $("#qr-result").css("border", "1px solid #07ba13");
                $("div#interactive").removeClass('full-screen');
                $("div.controls").html('<button class="start-qr">Start QR scanner</button>')
            }, 1000)
        }

        if (err) {
            if (err instanceof ZXing.NotFoundException) {
                console.log('No QR code found.')
            }

            if (err instanceof ZXing.ChecksumException) {
                console.log('A code was found, but it\'s read value was not valid.')
            }

            if (err instanceof ZXing.FormatException) {
                console.log('A code was found, but it was in a invalid format.')
            }
        }
    })

    $("div.controls").html('<button class="stop-qr">Stop QR scanner</button>')
});

function closeCamera(e) {
    e.preventDefault();
    codeReader.stopStreams();
    $("div#interactive").html('');
    $("div#interactive").removeClass('full-screen');
    $("div.controls").html('<button class="start-qr">Start QR scanner</button>')
}

$(".controls").on("click", "button.stop-qr", closeCamera);
$("#interactive").on("click", closeCamera);

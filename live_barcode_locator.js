$(function() {
    var App = {
        init: function() {
            var self = this;

            this.attachListeners();
        },
        handleError: function(err) {
            console.log(err);
        },
        checkCapabilities: function() {
            var track = Quagga.CameraAccess.getActiveTrack();
            var capabilities = {};
            if (typeof track.getCapabilities === 'function') {
                capabilities = track.getCapabilities();
            }
            this.applySettingsVisibility('zoom', capabilities.zoom);
            this.applySettingsVisibility('torch', capabilities.torch);
        },
        updateOptionsForMediaRange: function(node, range) {
            var NUM_STEPS = 6;
            var stepSize = (range.max - range.min) / NUM_STEPS;
            var option;
            var value;
            while (node.firstChild) {
                node.removeChild(node.firstChild);
            }
            for (var i = 0; i <= NUM_STEPS; i++) {
                value = range.min + (stepSize * i);
                option = document.createElement('option');
                option.value = value;
                option.innerHTML = value;
                node.appendChild(option);
            }
        },
        applySettingsVisibility: function(setting, capability) {
            // depending on type of capability
            if (typeof capability === 'boolean') {
                var node = document.querySelector('input[name="settings_' + setting + '"]');
                if (node) {
                    node.parentNode.style.display = capability ? 'block' : 'none';
                }
                return;
            }
            if (window.MediaSettingsRange && capability instanceof window.MediaSettingsRange) {
                var node = document.querySelector('select[name="settings_' + setting + '"]');
                if (node) {
                    this.updateOptionsForMediaRange(node, capability);
                    node.parentNode.style.display = 'block';
                }
                return;
            }
        },
        attachListeners: function() {
            var self = this;

            function closeCamera(e) {
                e.preventDefault();
                Quagga.stop();
                $("div#interactive").html("");
                $("#interactive").removeClass('full-screen');
                $("div.controls").html('<button class="start-barcode">Start barcode scanner</button>')
            }

            $(".controls").on("click", "button.stop-barcode", closeCamera);

            $("#interactive").on("click", closeCamera);

            $(".controls").on("click", "button.start-barcode", function(e) {
                e.preventDefault();
                var config = {
                    inputStream: {
                        type : "LiveStream",
                        constraints: {
                            width: {min: 640},
                            height: {min: 480},
                            facingMode: "environment",
                            aspectRatio: {min: 1, max: 2}
                        }
                    },
                    locator: {
                        patchSize: "medium",
                        halfSample: true
                    },
                    numOfWorkers: 2,
                    frequency: 10,
                    decoder: {
                        readers : [{
                            format: "code_39_reader",
                            config: {}
                        }]
                    },
                    locate: true
                };
                
                Quagga.init(config, function(err) {
                    if (err) {
                        return self.handleError(err);
                    }
                    self.checkCapabilities();
                    Quagga.start();
                });

                Quagga.onProcessed(function(result) {
                    var drawingCtx = Quagga.canvas.ctx.overlay,
                        drawingCanvas = Quagga.canvas.dom.overlay;
            
                    if (result) {
                        if (result.boxes) {
                            drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
                            result.boxes.filter(function (box) {
                                return box !== result.box;
                            }).forEach(function (box) {
                                Quagga.ImageDebug.drawPath(box, {x: 0, y: 1}, drawingCtx, {color: "green", lineWidth: 2});
                            });
                        }
            
                        if (result.box) {
                            Quagga.ImageDebug.drawPath(result.box, {x: 0, y: 1}, drawingCtx, {color: "#00F", lineWidth: 2});
                        }
            
                        if (result.codeResult && result.codeResult.code) {
                            Quagga.ImageDebug.drawPath(result.line, {x: 'x', y: 'y'}, drawingCtx, {color: 'red', lineWidth: 3});
                        }
                    }
                });
            
                Quagga.onDetected(function(result) {
                    var code = result.codeResult.code;
                    App.lastResult = code;
                    var $node = null, canvas = Quagga.canvas.dom.image;
        
                    $node = $('<li><div class="caption"><h4 class="code"></h4></div></div></li>');
                    $node.find("h4.code").html(code);
                    $("#barcode-result").val(code);
                    $("#barcode-result").css("border", "1px solid #07ba13");

                    setTimeout(() => {
                        Quagga.stop();
                        $("div#interactive").html("");
                        $("#interactive").removeClass('full-screen');
                        $("div.controls").html('<button class="start-barcode">Start barcode scanner</button>')
                    }, 1000);
                });
                $("#interactive").addClass('full-screen');
                $("div.controls").html('<button class="stop-barcode">Stop barcode scanner</button>')
            });
        },
        _accessByPath: function(obj, path, val) {
            var parts = path.split('.'),
                depth = parts.length,
                setter = (typeof val !== "undefined") ? true : false;

            return parts.reduce(function(o, key, i) {
                if (setter && (i + 1) === depth) {
                    if (typeof o[key] === "object" && typeof val === "object") {
                        Object.assign(o[key], val);
                    } else {
                        o[key] = val;
                    }
                }
                return key in o ? o[key] : {};
            }, obj);
        },
        detachListeners: function() {
            $(".controls").off("click", "button.stop-barcode");
            $(".controls").off("click", "button.start-barcode");
        },
        lastResult : null
    };

    App.init();
});
